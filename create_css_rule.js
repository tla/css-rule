/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * css-rule
 * @see {@link https://framagit.org/tla/css-rule}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/css-rule/blob/master/LICENSE}
 *
 * Create CSS rules without Tree manipulation
 *
 * @method css.add
 * @argument {String} selector - css selector
 * @argument {String} property - css property
 * @return void
 *
 * @method css.remove
 * @argument {String} selector - css selector
 * @return void
 *
 * @method css.reset
 * @return void
 *
 * @example
 *    var css = new cssgen();
 *    css.add( 'p' , 'color:red' );
 *    css.add( 'a' , 'color:blue; text-decoration:underline' );
 *    css.remove( 'p' ); // remove only ^P$ selector
 *    css.destroy(); // remove all
 *
 * requirement :
 *    - chrome ( desktop & android ) 1.0+
 *    - opera ( desktop and mobile ) 1.0+
 *    - IE ( desktop & mobile ) 5+
 *    - safari ( osx & ios ) 1.0+
 *    - firefox 1.0+
 * */

window.cssgen = ( function () {
    
    function exist ( _ ) {
        return _ !== undefined && _ !== null;
    }
    
    function isfunction ( _ ) {
        return exist( _ ) && typeof _ === 'function';
    }
    
    function sniffer ( instance , callback ) {
        var random = '_' + Math.random().toString( 30 ).substring( 2 ) ,
            element = instance.document.createElement( 'div' );
        
        element.id = random;
        element.style.setProperty( 'width' , '10px' );
        instance.document.body.appendChild( element );
        
        function check () {
            if ( element.clientWidth == 20 ) {
                callback();
                instance.remove( '#' + random );
                return instance.document.body.removeChild( element );
            }
            
            setTimeout( check , 10 );
        }
        
        instance.add( '#' + random , 'width: 20px !important;' );
        setTimeout( check , 10 );
    }
    
    function cssgen ( doc ) {
        !doc && ( doc = document );
        
        this.id = 0;
        this.index = {};
        this.timeout = [];
        this.document = doc;
        
        this.style = doc.createElement( 'STYLE' );
        this.style.setAttribute( 'type' , 'text/css' );
        this.style.appendChild( doc.createTextNode( "" ) );
        
        this.head = doc.getElementsByTagName( 'HEAD' )[ 0 ];
        this.head.appendChild( this.style );
        
        this.api = this.style.sheet;
        this.rules = this.api.cssRules;
        this.insertRule = 'insertRule' in this.api ? 'insertRule' : 'addRule';
        this.deleteRule = 'deleteRule' in this.api ? 'deleteRule' : 'removeRule';
    }
    
    cssgen.prototype.setTimeout = function ( fn , time ) {
        var id = setTimeout( function () {
            this.timeout.splice( this.timeout.indexOf( id ) , 1 );
            fn();
        } , time );
        this.timeout.push( id );
    };
    
    cssgen.prototype.clearTimeout = function () {
        this.timeout.forEach( function ( id ) {
            clearTimeout( id );
        } );
    };
    
    cssgen.prototype.removeByRule = function ( rule ) {
        var rules = Array.apply( null , this.rules ) , i;
        
        if ( ( i = rules.indexOf( rule ) , i > -1 ) ) {
            this.api[ this.deleteRule ]( i );
            
            return true;
        }
        
        return false;
    };
    
    /**
     * @method cssgen.prototype.add
     * @param {string} selector
     * @param {string} rules
     * @param {function} [call]
     * @return {void}
     * */
    cssgen.prototype.add = function ( selector , rules , call ) {
        var rule;
        
        this.api[ this.insertRule ]( selector + '{' + rules + '}' , this.rules.length );
        
        rule = this.rules[ this.rules.length - 1 ];
        
        if ( !this.index[ selector ] ) {
            this.index[ selector ] = [];
        }
        
        this.index[ selector ].push( rule );
        
        if ( isfunction( call ) ) {
            sniffer( this , call );
        }
    };
    
    /**
     * @method cssgen.prototype.remove
     * @param {string} selector
     * @return {boolean}
     * */
    cssgen.prototype.remove = function ( selector ) {
        var result = true , array , i , l;
        
        if ( array = this.index[ selector ] ) {
            for ( i = 0, l = array.length ; i < l ; ) {
                if ( !this.removeByRule( array[ i++ ] ) ) {
                    result = false;
                }
            }
            
            delete this.index[ selector ];
        }
        
        return result;
    };
    
    /**
     * @method cssgen.prototype.toggle
     * @param {string} selector
     * @param {string} rules
     * @param {function} [call]
     * @return {void}
     * */
    cssgen.prototype.toggle = function ( selector , rules , call ) {
        this.remove( selector );
        this.add( selector , rules , call );
    };
    
    /**
     * @method cssgen.prototype.destroy
     * @return {void}
     * */
    cssgen.prototype.destroy = function () {
        this.clearTimeout();
        this.head.removeChild( this.style );
    };
    
    return cssgen;
    
} )();